<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\ModelContoh;

class ControllerPelatihan extends Controller
{
    //
    public function hello($nama)
    {
    	echo 'Hello '.$nama.'. Ini dari Controller.';
    }

    public function index()
    {
    	return view('contohInput');
    }

    public function displayData()
    {
    	$data = ModelContoh::all();
    	return view('showData', ['data' => $data]);
    }

    public function displayDataByNIM($nim)
    {
    	$data = ModelContoh::where('NIM', $nim) ->first();
    	return view('showData', ['dataByNIM' => $data]);
    }

    public function insertContoh(Request $data)
    {
    	$dataBerhasil = ModelContoh::create(['NIM'=>$data->input('nim'),'NAMA'=>$data->input('nama')]);

    	$dataView = ModelContoh::all();
    	return view('showData', ['data' => $dataView]);
    }

    public function gotoUpdate($nim)
    {
    	$data = ModelContoh::where('NIM', $nim) ->first();
    	return view('contohEdit', ['dataByNIM' => $data]);    	
    }

    public function updateContoh(Request $data)
    {
    	$dataBerhasil = ModelContoh::where('NIM', $data->input('nim'))
    	->update(['NAMA'=>$data->input('nama')]);

    	$dataView = ModelContoh::all();
    	return view('showData', ['data' => $dataView]);
    	
    }

    public function deleteContoh($nim)
    {
    	$dataBerhasil = ModelContoh::where('NIM', $nim)->delete();

    	$dataView = ModelContoh::all();
    	return view('showData', ['data' => $dataView]);
    }
}
