<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'ControllerPelatihan@index');

Route::get('/hello/{nama}', function ($nama) {
    echo 'Hello '.$nama;
});

Route::get('/hello1/{nama}', 'ControllerPelatihan@hello');

Route::get('/showData', 'ControllerPelatihan@displayData');
Route::get('/showData/{nim}', 'ControllerPelatihan@displayDataByNIM');

Route::post('/createInput','ControllerPelatihan@insertContoh');
Route::get('/gotoUpdateInput/{nim}','ControllerPelatihan@gotoUpdate');
Route::post('/updateInput','ControllerPelatihan@updateContoh');
Route::get('/deleteInput/{nim}','ControllerPelatihan@deleteContoh');