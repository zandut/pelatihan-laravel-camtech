<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModelContoh extends Model
{
    //
    protected $table = 'contoh';
    protected $fillable = ['NIM', 'NAMA'];

    public $timestamps = false;
}
