<!DOCTYPE html>
<html>
<head>
	<title>Show Data</title>
</head>
<body>
	<table>
		<thead>
			<tr>
				<td>NIM</td>
				<td>NAMA</td>
				<td>ACTION</td>
			</tr>
		</thead>

		<tbody>
			<?php 

				if (isset($data))
				{
					foreach ($data as $key => $value) 
					{
						# code...
				
			 ?>

					 	<tr>
					 		<td><?php echo $value->NIM; ?></td>
					 		<td><?php echo $value->NAMA; ?></td>
					 		<td><a href="/gotoUpdateInput/<?php echo $value->NIM; ?>" style="margin-right: 10px">Update</a>
					 		<a href="/deleteInput/<?php echo $value->NIM; ?>" >Delete</a></td>
					 	</tr>

			 <?php 
			 		}
			 	}

			  ?>
		</tbody>

	</table>

	<?php 
		if (isset($dataByNIM)) 
		{

	?>
			<?php echo 'NIM : '.$dataByNIM->NIM; ?>
			<br>
			<?php echo 'NAMA : '.$dataByNIM->NAMA; ?>
	<?php 
		}
	 ?>

	 <a href="/">Input Data</a>
</body>
</html>